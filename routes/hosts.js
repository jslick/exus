const express = require('express')
const router = express.Router()

const { isAuthenticated } = require('../lib/auth-helpers')
const storage = require('../lib/storage')

function checkPageLoad(req, res, next) {
    if (req.xhr || req.headers.accept.indexOf('json') > -1)
        return next()

    res.sendFile(require('path').join(__dirname, '../public/index.html'))
}

router.get('/', [ isAuthenticated, checkPageLoad ], async (req,res,next) => {
    const da = storage.getDataAccess()
    res.send(JSON.stringify(await da.getHostList())).end()
})

module.exports = router
