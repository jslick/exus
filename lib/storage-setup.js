function setupStorage(app) {
    const BasicStorageAdapter = require('./storage/basic-storage-adapter')
    const adapter = new BasicStorageAdapter('./data.json')
    require('./storage').setDataAccess(adapter)
}

module.exports = setupStorage
