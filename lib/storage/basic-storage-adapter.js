const fse = require('fs-extra')

class BasicStorageAdapter {
    constructor(filename) {
        this._loadingFile = fse.readFile(filename, 'utf8').then(contents => {
            this._data = JSON.parse(contents)
        })
    }

    async getHostList() {
        await this._loadingFile
        return this._data.hosts.slice()
    }
}

module.exports = BasicStorageAdapter
