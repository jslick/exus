let dataAccess

function setDataAccess(da) {
    dataAccess = da
}

function getDataAccess() {
    return dataAccess
}

module.exports = {
    setDataAccess,
    getDataAccess,
}
