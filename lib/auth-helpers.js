function isAuthenticated(req, res, next) {
    if (req.isAuthenticated())
        return next()

    if (req.xhr) {
        res.status(401)
        res.end()
    }
    else {
        // TODO:  pass redirect back path
        res.redirect('/login')
    }
}

module.exports = { isAuthenticated }
