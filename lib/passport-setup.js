const LocalStrategy = require('passport-local').Strategy

module.exports = setupPassport

function setupPassport(app, passport) {
    app.use(passport.initialize())
    app.use(passport.session())

    passport.serializeUser((user,done) => {
        done(null, user.id)
    })

    passport.deserializeUser((id,done) => {
        done(null, { id: 1, username: 'admin' })
    })

    passport.use(new LocalStrategy((username,password,done) => {
        done(null, { id: 1, username: 'admin' })
    }))
}
