const express = require('express')
const path = require('path')
const cookieParser = require('cookie-parser')
const logger = require('morgan')
const session = require('express-session')
const passport = require('passport')

const setupPassport = require('./lib/passport-setup')
const setupStorage = require('./lib/storage-setup')
const { isAuthenticated } = require('./lib/auth-helpers')
const indexRouter = require('./routes/index')
//const usersRouter = require('./routes/users')
const hostsRouter = require('./routes/hosts')

const app = express()

app.use(logger('dev'))
app.use(express.json())
app.use(express.urlencoded({ extended: false }))
app.use(cookieParser())
app.use(session({
    secret: 'uiee8gf9iut',  // TODO
    resave: false,
    saveUninitialized:  false,
}))
setupPassport(app, passport)
app.use(express.static(path.join(__dirname, 'public')))

setupStorage(app)

app.get('/login', (req,res) => {
    const fs = require('fs')
    fs.readFile('./public/login.html', 'utf8', (err,contents) => {
        res.type('html').send(contents)
    })
})

app.post('/login', passport.authenticate('local'), (req,res) => {
    res.status(200)
    res.end()
})

app.get('/logout', (req,res,next) => {
    req.logout()
    req.session.save(err => {
        if (err) {
            return next(err)
        }
        res.redirect('/')
    })
})

app.use('/', indexRouter)
//app.use('/users', usersRouter)
app.use('/hosts', hostsRouter)

app.get('/test', isAuthenticated, (req,res) => {
    res.send('/test authenticated')
})

module.exports = app
