import axios from 'axios'

class ApiService {
    static async get(entityName) {
        // TODO:  OTHER PARAMS
        return axios.get(`/${ entityName }`)
    }
}

export default
class HostsService {
    static async getList() {
        const response = await ApiService.get('hosts')
        return response.data
    }
}
