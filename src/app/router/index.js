import Vue from 'vue'
import VueRouter from 'vue-router'

import Hosts from '../components/Hosts.vue'

Vue.use(VueRouter)

// TODO:  EXTRACT:
const NotFound = { template: '<div>Not found</div>' }

const routes = [
    { path: '/',        redirect: '/hosts' },
    { path: '/hosts',   component: Hosts },
    { path: '*',        component: NotFound },
]

const router = new VueRouter({
    mode:   'history',
    routes,
})
// TODO:  remove:
router.beforeEach((to,from,next) => {
    console.log(to.path)
    next()
})

export default router
