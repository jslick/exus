import { FETCH_HOSTS } from './actions'
import { SET_HOSTS } from './mutations'

import HostsService from '../data-access/hosts-service'

const state = {
    hosts:  [],
}

const actions = {
    async [FETCH_HOSTS]({ commit }) {
        console.log('Fetching hosts')
        const hosts = await HostsService.getList()
        commit(SET_HOSTS, hosts)
    },
}

const mutations = {
    [SET_HOSTS](state, hosts) {
        state.hosts = hosts
    }
}

const getters = {
    hosts(state) {
        return state.hosts
    }
}

export default {
    state,
    actions,
    mutations,
    getters,
}
