import Vue from 'vue'
import Vuex from 'vuex'

import hosts from './hosts'

Vue.use(Vuex)

export default new Vuex.Store({
    modules:  {
        hosts,
    }
})
