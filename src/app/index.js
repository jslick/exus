import Vue from 'vue'
import Vuetify from 'vuetify'

import App from './App.vue'
import store from './store'
import router from './router'

import axios from 'axios'

axios.interceptors.request.use(config => config, error => Promise.reject(error))
axios.interceptors.response.use(response => response, error => {
    if (error.response.status === 401) {
        console.error('Not logged in')
        window.location.href = '/login'
    }

    return Promise.reject(error)
})

Vue.use(Vuetify)

document.addEventListener('DOMContentLoaded', () => {
    new Vue({
        store,
        router,
        render(h) { return h(App) }
    }).$mount('body')
})
