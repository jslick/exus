import axios from 'axios'

function $1(selector, scope=document) {
    return scope.querySelector(selector)
}

document.addEventListener('DOMContentLoaded', init)

function init() {
    const form = $1('#login')
    form.addEventListener('submit', evt => {
        evt.preventDefault()
        tryLogin(form)
    })
}

async function tryLogin(form) {
    const username = $1('input[name=username]', form).value
    const password = $1('input[name=password]', form).value
    try {
        await postLogin(username, password)
        window.location = '/'
        // TODO:  redirect to previous page
    }
    catch (e) {
        console.error(e)
        // TODO:  show error message
    }
}

function postLogin(username, password) {
    return axios.post('/login', {
        username,
        password,
    })
}
