import commonjs from 'rollup-plugin-commonjs'
import resolve from 'rollup-plugin-node-resolve'
import json from 'rollup-plugin-json'
import vue from 'rollup-plugin-vue'
import replace from 'rollup-plugin-replace'

export default [
    {
        input:  `src/app/index.js`,

        plugins:  getPlugins(),

        output:  {
            format:     'iife',
            file:       `./public/javascripts/app.js`,
            sourcemap:  true,
        }
    },
    {
        input:  `src/login/index.js`,

        plugins:  getPlugins(),

        output:  {
            format:     'iife',
            file:       `./public/javascripts/login.js`,
            sourcemap:  true,
        }
    },
]

function getPlugins() {
    return [
        resolve({
            browser: true,
        }),
        commonjs(),
        json(),
        vue(),
        replace({
            'process.env.NODE_ENV': JSON.stringify('production')
        }),
    ]
}
